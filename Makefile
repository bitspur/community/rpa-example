FIND ?= find
GIT ?= git
JUPYTER ?= env/bin/jupyter
MKDIR ?= mkdir
NBROBOT ?= env/bin/nbrobot
PIP ?= env/bin/pip
PYTHON ?= env/bin/python
ROBOT ?= env/bin/robot
SED ?= sed
TOUCH ?= touch
VIRTUALENV ?= virtualenv
WEBDRIVERMANAGER ?= env/bin/webdrivermanager

NOTEBOOKS ?= $(shell $(FIND) . -name '*.ipynb' -not -path "./env/*" | $(SED) 's|^\.\/||g')

.PHONY: install
install: sudo /usr/local/bin/geckodriver env/.make/actions/install
env/.make/actions/install: requirements.txt
	@$(VIRTUALENV) env
	@$(PIP) install -r requirements.txt
	@$(JUPYTER) labextension install jupyterlab_robotmode
	@$(JUPYTER) labextension install @jupyter-widgets/jupyterlab-manager
	@$(PYTHON) -m robotkernel.install
	@$(MKDIR) -p $(@D)
	@$(TOUCH) -m $@
/usr/local/bin/geckodriver:
	@sudo $(WEBDRIVERMANAGER) firefox chrome --linkpath /usr/local/bin

.PHONY: sudo
sudo:
	@sudo true

.PHONY: $(NOTEBOOKS)
$(NOTEBOOKS): env/.make/actions/install
	@$(NBROBOT) $@

.PHONY: start
start: env/.make/actions/install
	@$(JUPYTER) notebook

.PHONY: eject
eject:
	@$(JUPYTER) nbconvert --to script $(NOTEBOOK)

.PHONY: clean
clean:
	@$(GIT) clean -fXd
