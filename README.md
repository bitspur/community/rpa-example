# rpa-example

> example rpa using robot kernel and robot framework

https://robotframework.org

https://github.com/robots-from-jupyter/robotkernel

https://robots-from-jupyter.github.io/robotkernel

https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html

## Usage

### Edit robots with jupyter

```sh
make start
```

### Run jupyter notebook with robot framework

```sh
make examples/cat.ipynb
```

### Eject jupyter notebook as a robot

```sh
make eject NOTEBOOK=examples/cat.ipynb
```

### Run a robot

```sh
robot examples/cat.robot
```
