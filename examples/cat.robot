*** Settings ***

Library  SeleniumLibrary
Library  SeleniumScreenshots

*** Keywords ***

Open singleton browser
    [Documentation]  Open a browser or re-use the opened browser
    [Arguments]  ${url}=about:blank
    Open browser  ${url}  alias=singleton

*** Tasks ***

Show Google
    Open singleton browser  https://google.com
    Capture and crop page screenshot
    ...  google.png
    ...  css:IMG.lnXdpd

*** Tasks ***

Search cat images
    Open singleton browser
    ...  https://duckduckgo.com
    Input text
    ...  id:search_form_input_homepage
    ...  cat
    Press keys
    ...  id:search_form_input_homepage
    ...  ENTER
    Wait until element is visible
    ...  link:Images
    Click element
    ...  link:Images
    Wait until element is visible
    ...  css:.tile--img__media
    Click element
    ...  css:.tile--img__media
    Wait until element is visible
    ...  css:.c-detail__btn
    Click element
    ...  css:.c-detail__btn
    Capture and crop page screenshot
    ...  cat.png
    ...  css:.js-detail-img-high


